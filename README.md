# Irrigation System
As a irrigation system which helps the automatic irrigation of agricultural lands without human intervention, system has to
be designed to fulfil the requirement of maintaining and configuring the plots of land by the irrigation time slots and the
amount of water required for each irrigation period.
The irrigation system should have integration interface with a sensor device to direct letting the sensor to irrigate based on
the configured time slots/amount of water.

## Features:

* RESTful APIs with HATEOAS
* Create Plot Land
* Update Plot Land
* Configure Plot Land Slots
* Configure Plot Land Sensors
* Integration interface with the sensor device once a plot of land need to be irrigate 
* Alerting System
* Predict the (slots time / amount of water) based on the given type of agricultural crop / cultivated area.
* Retry configurable through Kubernetes ConfigMap
* Seed Data 
* Dockerfile and docker-compose yml
* Cloud Native ready to be deployed on **Kubernetes**
* CI/CD Pipeline with **jenkinsfile**
* Swagger API Documentation

# Getting Started

First, Make sure you already installed Apache Maven, MongoDB, Docker, and docker-compose on your environment, then run:

#### For Windows
`./mvnw spring-boot:run`

#### For Linux
`./mvn spring-boot:run`

### Or With docker-compose

`./mvnw clean install -Ddb=db && docker-compose up`

#### To skip tests:

`./mvnw clean install -Ddb=db -DskipTests && docker-compose up`

To see the full documentation of the API go to [Swagger](http://localhost:8080/irrigation/swagger-ui/index.html)
also, you can try the API from swagger directly
![alt text](diagrams/swagger.png)
## Software Architecture and System Design

### System Context diagram

![alt text](diagrams/Irrigation-system-context.png)

As you can see Irrigation System has integration interfaces with two other systems:
* Notification System
* Sensors

### Container diagram

![alt text](diagrams/Irrigation-container-diagram.png)

The Irrigation System has three main containers 
* Web Application
* Backend Microservice
* Database
User can manager plot lands and configure them througth the web application, the web application integrate with backend micorservice using REST interface

### Component diagram

![alt text](diagrams/Irrigation-component-diagram.png)

The Irrigation System has 4 RestControllers 
* Plot Land Management API
* Configure Plot Land Slots API
* Configure Plot Land Sensors API
* Plot Land Irrigation API
* Slot time/ Amount of water Prediction API

These RestControllers integrate with the database througth Spring Mongo Repository

### Class diagram

![alt text](diagrams/class-diagram.png)

## How to use the Irrigation system?

### Using API:

#### Create Plot land
`curl -X POST "http://localhost:8080/irrigation/plot-lands/" -H "accept: */*" -H "Content-Type: application/json" -d '{ "agriculturalCrop": "Wheat", "code": "1", "cultivatedArea": 100, "latitude": 79.61667, "longitude": -162.25830, "name": "Wheat Land", "sensors": [ "1", "2" ], "slots": [ "13:00:00", "01:00:00" ], "waterAmount": 3000 }'`

#### List Plot lands

`curl -X GET "http://localhost:8080/irrigation/plot-lands/" -H "accept: */*"`

#### Update Plot land

`curl -X PUT "http://localhost:8080/irrigation/plot-lands/1" -H "accept: */*" -H "Content-Type: application/json" -d '{ "agriculturalCrop": "Wheat", "code": "1", "cultivatedArea": 100, "latitude": 79.61667, "longitude": -162.25830, "name": "Wheat Land", "sensors": [ "1", "2" ], "slots": [ "13:00:00", "01:00:00" ], "waterAmount": 3000 }'`

#### Configure Plot Land Slots

`curl -X PUT "http://localhost:8080/irrigation/plot-lands/1/slots/" -H "accept: */*" -H "Content-Type: application/json" -d '[ "12:00:00"]'`

#### Configure Plot Land Sensors

`curl -X PUT "http://localhost:8080/irrigation/plot-lands/1/sensors/" -H "accept: */*" -H "Content-Type: application/json" -d '[ "sensor_1"]'`

#### Slot time/ Amount of water prediction

`curl -X GET "http://localhost:8080/irrigation/predict/?agriculturalCrop=Wheat&cultivatedArea=50" -H "accept: */*"`