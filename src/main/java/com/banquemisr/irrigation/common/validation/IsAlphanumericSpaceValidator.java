package com.banquemisr.irrigation.common.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class IsAlphanumericSpaceValidator implements ConstraintValidator<IsAlphanumericSpace, String> {

    @Override
    public boolean isValid(String input, ConstraintValidatorContext constraintValidatorContext) {
        return input == null || (input != null && !input.isBlank() && StringUtils.isAlphanumericSpace(input));
    }
}
