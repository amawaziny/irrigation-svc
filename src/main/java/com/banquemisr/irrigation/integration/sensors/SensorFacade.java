package com.banquemisr.irrigation.integration.sensors;

import java.util.HashMap;
import java.util.Map;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.banquemisr.irrigation.common.exceptions.SensorNotAvailable;

@Service
public class SensorFacade {

    private Map<String, Sensor> sensors;

    public SensorFacade() {
        sensors = new HashMap<>();
        sensors.put("1", new Sensor("1", "ENDPOINT_URL"));
        sensors.put("2", new Sensor("2", "ENDPOINT_URL"));
        sensors.put("3", new Sensor("3", "ENDPOINT_URL"));
    }

    @Retryable(value = SensorNotAvailable.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    public void sendWaterAmount(String sensorCode, float waterAmount) throws SensorNotAvailable {
        if (sensors.containsKey(sensorCode)) {
            sensors.get(sensorCode).setWaterAmount(waterAmount);
        } else {
            throw new SensorNotAvailable(sensorCode);
        }
    }
}