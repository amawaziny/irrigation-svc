package com.banquemisr.irrigation.integration.notification;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationFacade {

    /**
     * Integration with another microservice (Notification microservice) through an
     * event bus or message queue to send an alert
     * 
     * @param message
     */
    @Async
    public void alert(String message) {
        log.warn(message);
    }
}