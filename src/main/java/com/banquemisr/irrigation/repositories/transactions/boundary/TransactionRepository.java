package com.banquemisr.irrigation.repositories.transactions.boundary;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.banquemisr.irrigation.repositories.transactions.entity.TransactionDocument;

public interface TransactionRepository extends MongoRepository<TransactionDocument, String> {
}
