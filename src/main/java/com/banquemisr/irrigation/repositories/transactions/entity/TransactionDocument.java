package com.banquemisr.irrigation.repositories.transactions.entity;

import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("transactions")
public class TransactionDocument {

    @Id
    private ObjectId id;
    private String plotLandId;
    private String sensorCode;
    private String slot;
    private boolean status;
    @CreatedDate
    private LocalDateTime createdAt;

}
