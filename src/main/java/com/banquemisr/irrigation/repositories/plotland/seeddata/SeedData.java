package com.banquemisr.irrigation.repositories.plotland.seeddata;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.banquemisr.irrigation.common.Utilities;
import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Profile("!test")
public class SeedData {

    private final PlotLandRepository plotLandRepository;

    public SeedData(PlotLandRepository plotLandRepository) {
        this.plotLandRepository = plotLandRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void seedData() {
        try {
            plotLandRepository.deleteAll();

            InputStream in = new ClassPathResource("seed-data/plotLands.json").getInputStream();
            byte[] jsonData = StreamUtils.copyToByteArray(in);
            ObjectMapper mapper = Utilities.getObjectMapper();
            List<PlotLandDocument> plotDocuments = mapper.readValue(jsonData,
                    new TypeReference<List<PlotLandDocument>>() {
                    });

            plotLandRepository.saveAll(plotDocuments);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
