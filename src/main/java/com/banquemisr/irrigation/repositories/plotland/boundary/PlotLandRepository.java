package com.banquemisr.irrigation.repositories.plotland.boundary;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;

public interface PlotLandRepository extends MongoRepository<PlotLandDocument, String> {

    public Optional<PlotLandDocument> findByCode(String code);

    public Optional<List<PlotLandDocument>> findByAgriculturalCropAndCultivatedArea(String agriculturalCrop,
            float cultivatedArea);
}
