package com.banquemisr.irrigation.resources.plotland.boundary.docs;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Configure Plot Land Slots", tags = { "Configure Plot Land Slots API." })
public interface ISlotsResource {

    @ApiOperation(value = "List all plot land slots")
    public ResponseEntity<List<String>> getSlots(String code);

    @ApiOperation(value = "Update plot land slots")
    public ResponseEntity<PlotLandModel> updateSlots(String code, List<String> slots);
}
