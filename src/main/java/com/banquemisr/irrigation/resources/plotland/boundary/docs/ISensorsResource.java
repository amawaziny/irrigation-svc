package com.banquemisr.irrigation.resources.plotland.boundary.docs;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Configure Plot Land Sensors", tags = { "Configure Plot Land Sensors API." })
public interface ISensorsResource {

    @ApiOperation(value = "List all plot land sensors")
    public ResponseEntity<List<String>> getSensors(String code);

    @ApiOperation(value = "Update plot land sensors")
    public ResponseEntity<PlotLandModel> updateSensors(String code, List<String> sensors);
}
