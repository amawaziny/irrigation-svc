package com.banquemisr.irrigation.resources.plotland.boundary.docs;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;

import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Plot Land Management", tags = { "Plot Land Management API." })
public interface IPlotLandsResource {

    @ApiOperation(value = "List all plots and it's details")
    public ResponseEntity<CollectionModel<EntityModel<PlotLandModel>>> findAll();

    @ApiOperation(value = "Find Plot and it's details by code")
    public ResponseEntity<EntityModel<PlotLandModel>> findOneByCode(String code);

    @ApiOperation(value = "Add new plot of land")
    public ResponseEntity<PlotLandModel> createPlotLands(PlotLandModel plotLand);

    @ApiOperation(value = "Edit a plot of land")
    public ResponseEntity<PlotLandModel> updatePlotLands(PlotLandModel plotLand, String code);
}
