package com.banquemisr.irrigation.resources.plotland.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlotLandIrrigationModel implements Serializable {

    @NotBlank
    private String slot;

}
