package com.banquemisr.irrigation.resources.plotland.boundary;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.resources.plotland.boundary.docs.IPredictResource;
import com.banquemisr.irrigation.resources.plotland.control.PlotLandsCtrl;
import com.banquemisr.irrigation.resources.plotland.entity.PredictSlotWaterAmountModel;

@RestController
@CrossOrigin
@RequestMapping(value = "/predict")
public class PredictResource implements IPredictResource {

    private final PlotLandRepository plotLandRepository;
    private final PlotLandsCtrl plotLandsCtrl;

    public PredictResource(PlotLandRepository plotLandRepository, PlotLandsCtrl plotLandsCtrl) {
        this.plotLandRepository = plotLandRepository;
        this.plotLandsCtrl = plotLandsCtrl;
    }

    @GetMapping("/")
    public ResponseEntity<PredictSlotWaterAmountModel> predict(
            @RequestParam("agriculturalCrop") String agriculturalCrop,
            @RequestParam("cultivatedArea") float cultivatedArea) {
        Optional<List<PlotLandDocument>> plotLandDocumentOptional = plotLandRepository
                .findByAgriculturalCropAndCultivatedArea(agriculturalCrop, 100);

        if (plotLandDocumentOptional.isPresent()) {
            List<PlotLandDocument> plotLandDocuments = plotLandDocumentOptional.get();
            if (!plotLandDocuments.isEmpty()) {
                PlotLandDocument plotLandDocument = plotLandDocuments.get(0);
                return ResponseEntity.ok(plotLandsCtrl.getPredictedValue(plotLandDocument, cultivatedArea));
            }
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.noContent().build();
        }
    }
}
