package com.banquemisr.irrigation.resources.plotland.control;

import java.util.List;

import org.springframework.stereotype.Service;

import com.banquemisr.irrigation.common.Utilities;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;
import com.banquemisr.irrigation.resources.plotland.entity.PredictSlotWaterAmountModel;
import com.fasterxml.jackson.core.type.TypeReference;

@Service
public class PlotLandsCtrl {

    public List<PlotLandModel> getPlotLandModels(List<PlotLandDocument> plotLandDocuments) {
        return Utilities.getObjectMapper().convertValue(plotLandDocuments,
                new TypeReference<List<PlotLandModel>>() {
                });
    }

    public PlotLandModel getPlotLandModel(PlotLandDocument plotLandDocument) {
        return Utilities.getObjectMapper().convertValue(plotLandDocument, PlotLandModel.class);
    }

    public PlotLandDocument getPlotLandDocument(PlotLandModel plotLandModel) {
        return Utilities.getObjectMapper().convertValue(plotLandModel, PlotLandDocument.class);
    }

    public PredictSlotWaterAmountModel getPredictedValue(PlotLandDocument plotLandDocument, float cultivatedArea) {
        float waterAmountPer100 = plotLandDocument.getWaterAmount() / 100;
        float waterAmount = waterAmountPer100 * cultivatedArea;
        return PredictSlotWaterAmountModel.builder()
                .slot(plotLandDocument.getSlots())
                .waterAmount(waterAmount)
                .build();
    }
}
