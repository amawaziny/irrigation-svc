package com.banquemisr.irrigation.resources.plotland.entity;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PredictSlotWaterAmountModel implements Serializable {

    private List<String> slot;
    private float waterAmount;
}
