package com.banquemisr.irrigation.resources.plotland.boundary;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.resources.plotland.boundary.docs.ISlotsResource;
import com.banquemisr.irrigation.resources.plotland.control.PlotLandsCtrl;
import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;

@RestController
@CrossOrigin
@RequestMapping(value = "/plot-lands/{code}/slots")
public class SlotsResource implements ISlotsResource {

    private final PlotLandRepository plotLandRepository;
    private final PlotLandsCtrl plotLandsCtrl;

    public SlotsResource(PlotLandRepository plotLandRepository, PlotLandsCtrl plotLandsCtrl) {
        this.plotLandRepository = plotLandRepository;
        this.plotLandsCtrl = plotLandsCtrl;
    }

    @GetMapping("/")
    public ResponseEntity<List<String>> getSlots(@PathVariable String code) {
        return plotLandRepository.findByCode(code)
                .map(p -> ResponseEntity.ok(p.getSlots()))
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/")
    public ResponseEntity<PlotLandModel> updateSlots(@PathVariable String code,
            @RequestBody @Valid @Size(min = 1) List<String> slots) {
        Optional<PlotLandDocument> plotlandDocOptional = plotLandRepository.findByCode(code);
        if (plotlandDocOptional.isPresent()) {
            PlotLandDocument plotLandDocument = plotlandDocOptional.get();
            plotLandDocument.setSlots(slots);
            plotLandDocument = plotLandRepository.save(plotLandDocument);
            return ResponseEntity.ok(plotLandsCtrl.getPlotLandModel(plotLandDocument));
        } else
            return ResponseEntity.notFound().build();
    }
}
