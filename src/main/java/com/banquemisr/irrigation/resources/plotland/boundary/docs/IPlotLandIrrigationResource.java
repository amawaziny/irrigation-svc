package com.banquemisr.irrigation.resources.plotland.boundary.docs;

import org.springframework.http.ResponseEntity;

import com.banquemisr.irrigation.resources.plotland.entity.PlotLandIrrigationModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Plot Land Irrigation", tags = { "Plot Land Irrigation API." })
public interface IPlotLandIrrigationResource {

    @ApiOperation(value = "Integration interface with the sensor device once a plot of land need to be irrigate")
    public ResponseEntity<Void> irrigate(PlotLandIrrigationModel slot, String code);
}
