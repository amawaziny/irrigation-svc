package com.banquemisr.irrigation.resources.plotland.boundary.docs;

import org.springframework.http.ResponseEntity;

import com.banquemisr.irrigation.resources.plotland.entity.PredictSlotWaterAmountModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Slot time/ Amount of water Prediction API.", tags = { "Slot time/ Amount of water Prediction API." })
public interface IPredictResource {

    @ApiOperation(value = "Predict the (slots time / amount of water) based on the given type of agricultural crop / cultivated area")
    public ResponseEntity<PredictSlotWaterAmountModel> predict(String agriculturalCrop, float cultivatedArea);
}
