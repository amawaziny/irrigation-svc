package com.banquemisr.irrigation.resources.plotland.boundary;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StreamUtils;

import com.banquemisr.irrigation.AbstractIT;
import com.banquemisr.irrigation.common.Utilities;
import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
public class PlotLandsResourceIT extends AbstractIT {

	public static final String PLOT_LAND_ENDPOINT = "/plot-lands";

	@Autowired
	private PlotLandRepository plotLandRepository;

	@BeforeAll
	public void init() throws JsonParseException, JsonMappingException, IOException {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		plotLandRepository.deleteAll();

		InputStream in = new ClassPathResource("seed-data/plotLands.json").getInputStream();
		byte[] jsonData = StreamUtils.copyToByteArray(in);
		ObjectMapper mapper = Utilities.getObjectMapper();
		List<PlotLandDocument> plotDocuments = mapper.readValue(jsonData, new TypeReference<List<PlotLandDocument>>() {
		});

		plotLandRepository.saveAll(plotDocuments);
	}

	@Test
	@DisplayName("Test Create Plot Land")
	public void testCreatePlotLand() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		PlotLandModel plotLandModel = easyRandom.nextObject(PlotLandModel.class);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.post(PLOT_LAND_ENDPOINT + "/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapToJson(plotLandModel)))
				.andExpect(status().isCreated()).andReturn();

		MockHttpServletResponse mockHttpServletResponse = mvcResult.getResponse();
		assertEquals(mockHttpServletResponse.getStatus(), 201);
	}

	@Test
	@DisplayName("Test Update Plot Land")
	public void testUpdatePlotLand() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		PlotLandModel plotLandModel = easyRandom.nextObject(PlotLandModel.class);

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
				.put(PLOT_LAND_ENDPOINT + "/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapToJson(plotLandModel)))
				.andExpect(status().isOk()).andReturn();

		MockHttpServletResponse mockHttpServletResponse = mvcResult.getResponse();
		assertEquals(mockHttpServletResponse.getStatus(), 200);
	}
}
