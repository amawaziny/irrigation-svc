FROM openjdk:11-jdk-alpine
LABEL "MAINTAINER"=amawaziny@gmail.com
COPY target/irrigation-svc.jar irrigation-svc.jar
ENTRYPOINT ["java","-jar","/irrigation-svc.jar"]